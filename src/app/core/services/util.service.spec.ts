/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

import { UtilService } from '@core/services/util.service';

describe('UtilService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UtilService],
      imports: [HttpClientTestingModule]
    });
  });

  it('should be created', inject([UtilService], (service: UtilService) => {
    expect(service).toBeTruthy();
  }));

  it('should return a property from config file', inject([UtilService], (service: UtilService) => {
    expect(service.readConfig('production')).toEqual(false);
  }));

  it('should return a empty string if the property can´t be found', inject([UtilService], (service: UtilService) => {
    expect(service.readConfig('test')).toEqual('');
  }));
});

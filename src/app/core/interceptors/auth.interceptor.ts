/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { finalize, delay, map, tap } from 'rxjs/operators';
import { UtilService } from '@core/services/util.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private injector: Injector
  ) { }
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const utilService = this.injector.get(UtilService);
    utilService.throwError(undefined);
    req = req.clone({
      setHeaders: {
        Authorization: `Bearer ${localStorage.getItem('ACCESS_TOKEN')}`
      }
    });
    return next.handle(req).pipe(
      delay(0), // neccessary due to the spinner -> triggers a new round of change detection
      tap(() => {
        utilService.loadingState$.next(true);
      }),
      finalize(() => {
        utilService.loadingState$.next(false);
      })
    );
  }
}

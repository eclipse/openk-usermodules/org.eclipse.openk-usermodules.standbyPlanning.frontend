/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@shared/shared.module';
import { NavigationComponent } from '@core/components/navigation/navigation.component';
import { LogoutComponent } from '@core/components/logout/logout.component';
import { RedirectComponent, LOCATION_TOKEN } from '@core/components/redirect/redirect.component';
import { ErrorComponent } from './components/error/error.component';
import { AboutComponent } from './components/about/about.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { VersionInfoComponent } from './components/version-info/version-info.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    NavigationComponent,
    ErrorComponent,
    LoadingSpinnerComponent,
    VersionInfoComponent
  ],
  declarations: [
    NavigationComponent,
    LogoutComponent,
    RedirectComponent,
    ErrorComponent,
    AboutComponent,
    LoadingSpinnerComponent,
    VersionInfoComponent
  ],
  providers: [
    { provide: LOCATION_TOKEN, useValue: window.location }
  ]
})
export class CoreModule { }

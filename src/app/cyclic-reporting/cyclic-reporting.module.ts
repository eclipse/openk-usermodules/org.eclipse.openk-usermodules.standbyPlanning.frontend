/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {SharedModule} from '@shared/shared.module';
import {CyclicReportFormComponent} from './components/form/cyclic-report-form.component';
import {CyclicReportFormDateControlsComponent} from './components/form-date-controls/cyclic-report-form-date-controls.component';
import {CyclicReportFormInfoComponent} from './components/form-info/cyclic-report-form-info.component';
import {CyclicReportFormInputComponent} from './components/form-input/cyclic-report-form-input.component';
import {CyclicReportFormSelectComponent} from './components/form-select/cyclic-report-form-select.component';
import {CyclicReportsOverviewComponent} from './components/overview/cyclic-reports-overview.component';
import {CyclicReportFormTextareaComponent} from './components/form-textarea/cyclic-report-form-textarea.component';
import {CyclicReportingRoutingModule} from './cyclic-reporting-routing.module';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,

        CyclicReportingRoutingModule,
        SharedModule
    ],
  declarations: [
    CyclicReportFormComponent,
    CyclicReportFormDateControlsComponent,
    CyclicReportFormInfoComponent,
    CyclicReportFormInputComponent,
    CyclicReportFormSelectComponent,
    CyclicReportFormTextareaComponent,
    CyclicReportsOverviewComponent
  ]
})
export class CyclicReportingModule {

}

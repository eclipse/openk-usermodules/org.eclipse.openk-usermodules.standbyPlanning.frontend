/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {FormControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';
import {CyclicReportValidators} from './cyclic-report.validators';

function getValidationErrors<T>(...validators: ValidatorFn[]): (data: T) => ValidationErrors {
  return (data: T) => new FormControl(data, validators).errors;
}

describe('CyclicReportValidators', () => {

  it('validationDate should check if the validation date is correctly set', () => {
    const errors = { invalidDate: true };
    const getValidationDateError = getValidationErrors<Partial<CyclicReportObject>>(CyclicReportValidators.validationDate);

    expect(getValidationDateError(null)).toBe(null);
    expect(getValidationDateError({})).toBe(null);

    expect(getValidationDateError({
      validToDayOffset: 1, validToHour: 12, validToMinute: 30,
      validFromDayOffset: 0, validFromHour: 12, validFromMinute: 30
    })).toEqual(null);

    expect(getValidationDateError({
      validToDayOffset: 1, validToHour: 12, validToMinute: 30,
      validFromDayOffset: 1, validFromHour: 0, validFromMinute: 30
    })).toEqual(null);

    expect(getValidationDateError({
      validToDayOffset: 1, validToHour: 12, validToMinute: 30,
      validFromDayOffset: 1, validFromHour: 12, validFromMinute: 0
    })).toEqual(null);

    expect(getValidationDateError({
      validToDayOffset: 1, validToHour: 12, validToMinute: 30,
      validFromDayOffset: 1, validFromHour: 12, validFromMinute: 30
    })).toEqual(errors);

    expect(getValidationDateError({
      validToDayOffset: 1, validToHour: 12, validToMinute: 30,
      validFromDayOffset: 1, validFromHour: 14, validFromMinute: 0
    })).toEqual(errors);

    expect(getValidationDateError({
      validToDayOffset: 1, validToHour: 12, validToMinute: 30,
      validFromDayOffset: 12, validFromHour: 10, validFromMinute: 0
    })).toEqual(errors);

  });

});

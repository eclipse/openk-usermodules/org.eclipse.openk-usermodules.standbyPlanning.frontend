/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';
import {CyclicReportObject} from '@shared/model/CyclicReportObject';

export class CyclicReportValidators {

  public static validationDate: ValidatorFn = (control: AbstractControl): ValidationErrors => {
    const error = { invalidDate: true };

    if (control.value == null) {
      return;
    }

    const value: CyclicReportObject = control.value;
    const { validFromDayOffset, validFromHour, validFromMinute, validToDayOffset, validToHour, validToMinute} = value;

    if ( ![validFromDayOffset, validFromHour, validFromMinute, validToDayOffset, validToHour, validToMinute].every(Number.isInteger) ) {
      return;
    }

    const isLargerZero = (arg: number) => arg === 0 ? null : arg > 0;

    const isDayOffsetValid = isLargerZero(validToDayOffset - validFromDayOffset);
    const isHourValid = isLargerZero(validToHour - validFromHour);
    const isMinuteValid = isLargerZero(validToMinute - validFromMinute);

    if (isDayOffsetValid) {
      return;
    }

    if (isDayOffsetValid == null && isHourValid) {
      return;
    }

    if (isDayOffsetValid == null && isHourValid == null && isMinuteValid) {
      return;
    }

    return error;
  }

}

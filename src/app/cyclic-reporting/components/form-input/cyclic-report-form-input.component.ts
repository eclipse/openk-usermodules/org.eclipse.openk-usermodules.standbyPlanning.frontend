/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';

@Component({
  selector: 'ok-cyclic-report-form-input',
  styleUrls: ['cyclic-report-form-input.component.scss'],
  templateUrl: 'cyclic-report-form-input.component.html'
})
export class CyclicReportFormInputComponent {

  private static id = 0;

  @Input()
  public controlId = `CyclicReportFormInputComponent-${CyclicReportFormInputComponent.id++}`;

  @Input()
  public form: FormGroup | FormArray;

  @Input()
  public key: string | number;

  @Input()
  public displayAddButton: boolean;

  @Input()
  public displayCancelButton: boolean;

  @Output()
  public cancel = new EventEmitter<void>();

  @Output()
  public add = new EventEmitter<void>();

}

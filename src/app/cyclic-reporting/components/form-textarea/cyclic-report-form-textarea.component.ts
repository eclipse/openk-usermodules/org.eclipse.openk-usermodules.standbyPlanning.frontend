/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import {Component, Input} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';

@Component({
  selector: 'ok-cyclic-report-form-textarea',
  styleUrls: ['cyclic-report-form-textarea.component.scss'],
  templateUrl: 'cyclic-report-form-textarea.component.html'
})
export class CyclicReportFormTextareaComponent {

  private static id = 0;

  @Input()
  public controlId = `CyclicReportFormTextareaComponent-${CyclicReportFormTextareaComponent.id++}`;

  @Input()
  public form: FormGroup | FormArray;

  @Input()
  public key: string | number;

}

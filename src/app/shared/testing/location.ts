/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class LocationMockObjects {
    LOCATION_OBJECT = {
        id: 1,
        title: 'test',
        district: '',
        shorttext: 'test',
        community: 'test',
        lsPostcode: [{ id: '1', pcode: '33100' }],
        lsLocationForBranches: [],
        lsRegions: []
    };

    BRANCH_ARRAY = [{
        id: 1,
        title: 'Strom'
    }, {
        id: 2,
        title: 'Gas'
    }, {
        id: 3,
        title: 'Wasser'
    }];

    REGION_SELECTION_ARRAY = [{
        id: 1,
        regionName: 'RegionA'
    }, {
        id: 2,
        regionName: 'RegionB'
    }];

    POSTCODE_ARRAY = [{
        id: '1',
        pcode: '33100'
    }, {
        id: '2',
        pcode: '33101'
    }, {
        id: '3',
        pcode: '33102'
    }, {
        id: '4',
        pcode: '33103'
    }];

    LOCATION_ARRAY = [{
        title: 'test',
        district: 'test',
        shorttext: 'test',
        community: 'test'
    },
    {
        title: 'test',
        district: 'test',
        shorttext: 'test',
        community: 'test'
    }];
}

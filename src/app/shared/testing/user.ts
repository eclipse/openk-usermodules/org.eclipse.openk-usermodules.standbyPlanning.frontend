/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
export class UserMockObjects {
    USER_OBJECT = {
        id: 1,
        firstname: 'test',
        lastname: 'test',
        lsUserFunctions: [],
        lsUserInRegions: []
    };

    REGION_SELECTION_ARRAY = [{
        id: 1,
        regionName: 'RegionA'
    }, {
        id: 2,
        regionName: 'RegionB'
    }];

    FUNCTION_SELECTION_ARRAY = [{
        id: 1,
        functionName: 'LKW Fahrer'
    }, {
        id: 2,
        functionName: 'Elektriker'
    }];

    ADDRESS_OBJECT = {
        postcode: '33100',
        community: 'Paderborn',
        communitySuffix: 'Pad',
        street: 'Klingenderstraße',
        housenumber: '10-14',
        wgs84zone: '12315',
        latitude: '31351',
        longitude: '53151'
    };

    ORGANISATION_OBJECT = {
        id: 1,
        orgaName: 'test',
        address: this.ADDRESS_OBJECT
    };

    USER_ARRAY = [{
        firstname: 'test',
        lastname: 'test'
    },
    {
        firstname: 'test',
        lastname: 'test'
    }];
}

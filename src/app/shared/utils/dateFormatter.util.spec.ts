/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

import { NgbDateStringParserFormatter } from '@shared/utils/dateFormatter.util';

/*describe('DateFormatter', () => {
    describe('parsing', () => {
        it('should parse dates to correct format', () => {
            const dateFormatter = new NgbDateStringParserFormatter();
            const parsedDate: NgbDateStruct = dateFormatter.parse('10-09-18');
            expect(parsedDate).toBe({ day: 10, month: 09, year: 2018 });
        });

        it('should return null if the passed value is undefined', () => {
            const dateFormatter = new NgbDateStringParserFormatter();
            const parsedDate: NgbDateStruct = dateFormatter.parse(undefined);
            expect(parsedDate).toBe(null);
        });
    });
    describe('formatting', () => {
        it('should format a date given in NgbDateStruct format', () => {
            const dateFormatter = new NgbDateStringParserFormatter();
            const parsedDate: string = dateFormatter.format({ day: 10, month: 09, year: 2018 });
            expect(parsedDate).toBe('10-09-18');
        });
    });
});*/

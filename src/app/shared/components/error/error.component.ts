/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mettenmeier GmbH - initial implementation
 *   Basys GmbH - automatic report generation implementation
 ********************************************************************************/

import {Component, Input} from '@angular/core';
import {AbstractControl, AbstractControlDirective} from '@angular/forms';

@Component({
  selector: 'ok-error',
  templateUrl: './error.component.html',
  styleUrls: ['./error.component.scss']
})
export class ErrorComponent {

  private static readonly errorMessages = {
    required: () => 'Dieses Feld muss ausgefüllt werden.',
    minlength: (params) => `Es müssen mindestens ${params.requiredLength} Zeichen eingegeben werden`,
    maxlength: (params) => `Es dürfen maximal ${params.requiredLength} Zeichen eingegeben werden`,
    pattern: (params) => `Die Eingabe entspricht nicht dem Muster: ${params.requiredPattern}`,
    invalidDate: () => `Das "von"-Datum muss vor dem "bis"-Datum liegen.`,
    email: () => `Die Eingabe enthält keine gültige Emailadresse.`
  };

  @Input()
  private control: AbstractControlDirective | AbstractControl;

  public shouldShowErrors(): boolean {
    return this.control &&
      this.control.errors &&
      (this.control.dirty || this.control.touched);
  }

  public listOfErrors(): string[] {
    if (!this.control.errors) {
      return [];
    }
    return Object.keys(this.control.errors)
      .map(field => this.getMessage(field, this.control.errors[field]));
  }

  private getMessage(type: string, params: any) {
    if (type in ErrorComponent.errorMessages) {
      return ErrorComponent.errorMessages[type](params);
    }
    return '';
  }

}

/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, Input, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';

import { GridApi } from 'ag-grid-community';

import { AGGRID_LOCALETEXT, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-picklist-reactive',
  templateUrl: './picklist-reactive.component.html',
  styleUrls: ['./picklist-reactive.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PicklistReactiveComponent implements OnInit {
  @Input()
  sourceListColumnDefs: Array<any>;
  @Input()
  targetListColumnDefs: Array<any>;
  @Input()
  headlineSource: string;
  @Input()
  headlineTarget: string;
  @Input()
  sourceList: Array<any>;
  @Input()
  targetList: Array<any>;
  @Input()
  headerActionTargetLableLeft: string;
  @Input()
  headerActionTargetLableRight: string;
  @Input()
  id: string;
  @Input()
  rowSelection = 'multiple';
  @Input()
  showMultiMoveButtons = true;
  @Input()
  embedLocation: string;

  @Output()
  moveToTargetClicked: EventEmitter<any> = new EventEmitter();
  @Output()
  moveToSourceClicked: EventEmitter<any> = new EventEmitter();
  @Output()
  headerActionTargetClickedLeft: EventEmitter<any> = new EventEmitter();
  @Output()
  headerActionTargetClickedRight: EventEmitter<any> = new EventEmitter();

  defaultColDef = DEFAULT_COL_DEF;
  localeText = AGGRID_LOCALETEXT;

  gridSourceId: string;
  gridTargetId: string;
  btnMoveToTargetId: string;
  btnMoveAllToTargetId: string;
  btnMoveToSourceId: string;
  btnMoveAllToSourceId: string;


  /**
   * AG Grid
   */
  gridApiSource: GridApi;
  gridApiTarget: GridApi;

  constructor(
    public authService: AuthenticationService
  ) { }

  ngOnInit() {
    this.gridSourceId = this.id + 'GridSource';
    this.gridTargetId = this.id + 'GridTarget';
    this.btnMoveToTargetId = this.id + 'BtnToTarget';
    this.btnMoveAllToTargetId = this.id + 'BtnAllToTarget';
    this.btnMoveToSourceId = this.id + 'BtnToSource';
    this.btnMoveAllToSourceId = this.id + 'BtnAllToSource';
  }

  /**
   * AG Grid handling
   */
  onGridReadySource(params) {
    this.gridApiSource = params.api;
    this.gridApiSource.sizeColumnsToFit();
  }

  onGridReadyTarget(params) {
    this.gridApiTarget = params.api;
    this.gridApiTarget.sizeColumnsToFit();
  }

  /**
   * Picklist handling
   */

  /**
   * A modal dialog is opened on button click. Within this modal the user has to enter validity dates. Therefore a subscription
   * to the decision is made. If the subscription returns true, data is saved and moved over. If the subscription is false the
   * data is not moved and everything is as it was before.
   */
  moveToTarget(all: boolean) {
    if (all) {
      this.gridApiSource.selectAll();
    }

    // The map operator creates new objects within the error. Without that we can´t delete properties from the transfer
    // object without modifying the initial object
    const selectedRowsSource: Array<any> = this.gridApiSource.getSelectedRows().map(a => Object.assign({}, a));

    if (selectedRowsSource && selectedRowsSource.length) {
      this.moveToTargetClicked.emit(selectedRowsSource);
      this.gridApiSource.sizeColumnsToFit();
      this.gridApiTarget.sizeColumnsToFit();
      this.gridApiSource.deselectAll();
    }
  }

  moveToSource(all: boolean) {
    if (all) {
      this.gridApiTarget.selectAll();
    }

    const selectedRowsTarget: Array<any> = this.gridApiTarget.getSelectedRows().map(a => Object.assign({}, a));

    if (selectedRowsTarget && selectedRowsTarget.length) {
      this.moveToSourceClicked.emit(selectedRowsTarget);
      this.gridApiSource.sizeColumnsToFit();
      this.gridApiTarget.sizeColumnsToFit();
      this.gridApiTarget.deselectAll();
    }
  }

  headerActionTargetLeft() {
    const selectedRowsTarget: Array<any> = this.gridApiTarget.getSelectedRows().map(a => Object.assign({}, a));
    if (selectedRowsTarget && selectedRowsTarget.length) {
      this.headerActionTargetClickedLeft.emit(selectedRowsTarget);
    }
  }

  headerActionTargetRight() {
    const selectedRowsTarget: Array<any> = this.gridApiTarget.getSelectedRows().map(a => Object.assign({}, a));
    if (selectedRowsTarget && selectedRowsTarget.length) {
      this.headerActionTargetClickedRight.emit(selectedRowsTarget);
    }
  }
}

/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertComponent } from '@shared/components/alert/alert.component';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

describe('AlertComponent', () => {
  let component: AlertComponent;
  let fixture: ComponentFixture<AlertComponent>;
  let okButtonEl: DebugElement;
  let cancelButtonEl: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlertComponent]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    okButtonEl = fixture.debugElement.query(By.css('button#okButton'));
    cancelButtonEl = fixture.debugElement.query(By.css('button#cancelButton'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should emit truthy event on okButton click', () => {
    let decision: boolean;
    component.decision.subscribe(res => {
      decision = res;
    });

    okButtonEl.triggerEventHandler('click', null);
    expect(decision).toBeTruthy();
  });

  it('should emit falsy event on cancelButton click', () => {
    let decision: boolean;
    component.decision.subscribe(res => {
      decision = res;
    });

    cancelButtonEl.triggerEventHandler('click', null);
    expect(decision).toBeFalsy();
  });
});

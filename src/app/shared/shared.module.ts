/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH and others.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *   Mettenmeier GmbH - initial implementation
 *   Basys GmbH - automatic report generation implementation
 ********************************************************************************/

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {ReactiveFormsModule} from '@angular/forms';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AgGridModule} from 'ag-grid-angular';
import {ToastModule} from 'primeng/toast';
import {MenuModule} from 'primeng/menu';
import {DropdownModule} from 'primeng/dropdown';
import {MultiSelectModule} from 'primeng/multiselect';

import {CardComponent} from '@shared/components/card/card.component';
import {ErrorComponent} from '@shared/components/error/error.component';
import {AlertComponent} from '@shared/components/alert/alert.component';
import {PicklistReactiveComponent} from '@shared/components/picklist-reactive/picklist-reactive.component';
import {AbstractFormComponent} from './abstract/abstract-form/abstract-form.component';
import {InformationComponent} from './components/information/information.component';
import {AbstractDialogComponent} from './abstract/abstract-dialog/abstract-dialog.component';
import {StepperControlComponent} from '@shared/components/stepper-control/stepper-control.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule,
    ReactiveFormsModule,
    AgGridModule.withComponents([]),
    ToastModule,
    MenuModule,
    MultiSelectModule,
    DropdownModule
  ],
  exports: [
    NgbModule,
    RouterModule,
    CardComponent,
    ReactiveFormsModule,
    ErrorComponent,
    AlertComponent,
    AgGridModule,
    HttpClientModule,
    ToastModule,
    MenuModule,
    DropdownModule,
    MultiSelectModule,
    PicklistReactiveComponent,
    AbstractFormComponent,
    StepperControlComponent
  ],
  declarations: [
    CardComponent,
    ErrorComponent,
    AlertComponent,
    PicklistReactiveComponent,
    AbstractFormComponent,
    InformationComponent,
    AbstractDialogComponent,
    StepperControlComponent
  ],
  entryComponents: [
    AlertComponent,
    InformationComponent
  ]
})
export class SharedModule { }

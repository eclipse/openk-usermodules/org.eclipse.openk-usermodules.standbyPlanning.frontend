/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export class ArchiveObject {
    id: number;
    comment: string;
    modificationDate: any;
    modifiedBy: string;
    modifiedCause: string;
    title: string;
    jsonPlan: string;
    date?: {
        validFrom: any;
        validTo: any;
    };
    validFrom: any;
    validTo: any;
}

/********************************************************************************
 * Copyright © 2020 Basys GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

export interface CyclicReportObject {
  id: number;
  name: string;
  fileNamePattern: string;
  subject: string;
  to: string[];
  emailText: string;
  printFormat: string;
  reportName: string;
  standByListId: number;
  statusId: number;
  triggerWeekDay: number; // ISO-Weekday, i.e. 1 = Monday, ..., 7 = Sunday
  triggerHour: number;
  triggerMinute: number;
  validFromDayOffset: number;
  validFromHour: number;
  validFromMinute: number;
  validToDayOffset: number;
  validToHour: number;
  validToMinute: number;
}

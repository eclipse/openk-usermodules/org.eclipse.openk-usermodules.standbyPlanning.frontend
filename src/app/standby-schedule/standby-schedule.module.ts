/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StandbyScheduleRoutingModule } from './standby-schedule-routing.module';
import { SharedModule } from '@shared/shared.module';
import { OverviewComponent } from '@schedule/components/overview/overview.component';
import { PlanninglistComponent } from './components/planninglist/planninglist.component';
import { CalculateDialogComponent } from './dialogs/calculate-dialog/calculate-dialog.component';
import { ProtocolDialogComponent } from './dialogs/protocol-dialog/protocol-dialog.component';
import { TransferplanComponent } from './components/transferplan/transferplan.component';
import { TransferDialogComponent } from './dialogs/transfer-dialog/transfer-dialog.component';
import { ReplaceDialogComponent } from './dialogs/replace-dialog/replace-dialog.component';
import { MoveDialogComponent } from './dialogs/move-dialog/move-dialog.component';
import { ArchivelistComponent } from './components/archivelist/archivelist.component';
import { ArchiveComponent } from './components/archive/archive.component';
import { ArchiveDialogComponent } from './dialogs/archive-dialog/archive-dialog.component';
import { LoosingDataDialogComponent } from './dialogs/loosing-data-dialog/loosing-data-dialog.component';
import { DeleteDataDialogComponent } from './dialogs/delete-data-dialog/delete-data-dialog.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    StandbyScheduleRoutingModule
  ],
  declarations: [
    OverviewComponent,
    PlanninglistComponent,
    CalculateDialogComponent,
    ProtocolDialogComponent,
    TransferplanComponent,
    TransferDialogComponent,
    ReplaceDialogComponent,
    MoveDialogComponent,
    ArchivelistComponent,
    ArchiveComponent,
    ArchiveDialogComponent,
    LoosingDataDialogComponent,
    DeleteDataDialogComponent
  ],
  entryComponents: [
    CalculateDialogComponent,
    ProtocolDialogComponent,
    TransferDialogComponent,
    ReplaceDialogComponent,
    MoveDialogComponent,
    ArchiveDialogComponent,
    LoosingDataDialogComponent,
    DeleteDataDialogComponent
  ]
})
export class StandbyScheduleModule { }

/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PlanningService } from './planning.service';
import { UtilService } from '@core/services/util.service';
import { StandbylistObject } from '@shared/model/StandbylistObject';
import { SchedulePlanningObject } from '@shared/model/SchedulePlanningObject';
import { SearchBodiesObject } from '@shared/model/SearchBodiesObject';
import { MoveUserObject, ReplaceUserObject } from '@shared/model/PlanActionsObject';
import { SearchCurrentStandbyObject } from '@shared/model/SearchCurrentStandbyObject';

describe('PlanningService', () => {
  let planningService: PlanningService;
  let utilService: UtilService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        PlanningService,
        UtilService
      ]
    });

    planningService = TestBed.get(PlanningService);
    utilService = TestBed.get(UtilService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', inject([PlanningService], (service: PlanningService) => {
    expect(service).toBeTruthy();
  }));

  it('should get planning Data', () => {
    planningService.getPlanningData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/planning/list`);
  });

  it('should get the planning with id 1', () => {
    planningService.getPlanning(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/planning/1`);
  });

  it('should save a planning object', () => {
    const standbygroupObj = new SchedulePlanningObject();
    planningService.savePlanning(standbygroupObj).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/planning/save`);
  });

  it('should add the standbylists with id 1', () => {
    const standbyListArray = new StandbylistObject();
    planningService.addStandbylistPlanning(1, [standbyListArray]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/planning/1/standbylist/save/list`);
  });

  it('should delete the standbylists with id 1', () => {
    const standbyListArray = new StandbylistObject();
    planningService.deleteStandbylistPlanning(1, [standbyListArray]).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/planning/1/standbylist/delete/list`);
  });

  it('should start calculation with scheduleHeader', () => {
    const scheduleHeader = new SchedulePlanningObject();
    planningService.startCalculation(scheduleHeader).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/planning/blueprint/calculate`);
  });

  it('should filter bodies according to searchData', () => {
    const searchData = new SearchBodiesObject();
    planningService.filterBodies(1, searchData).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/filter/bodies/statusId/1/zip`);
  });

  /**
   * Transfer Bodies
   */
  it('should transfer bodies according to dataToTransfer', () => {
    const dataToTransfer = new SearchBodiesObject();
    planningService.transferBodies(dataToTransfer).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/copy/bodies`);
  });

  /**
   * Plan Actions
   */

  it('should move given user', () => {
    const data = new MoveUserObject();
    planningService.moveUser(data).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/planning/user/move`);
  });

  it('should replace given user', () => {
    const data = new ReplaceUserObject();
    planningService.replaceUser(data).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/planning/user/replace`);
  });

  /**
   * Archive Plan
   */

  it('should archive bodies according to searchData', () => {
    const searchData = new SearchBodiesObject();
    planningService.archivePlan(1, searchData).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/archive/standbylist/1`);
  });

  it('should get list of archive data', () => {
    planningService.getArchiveData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/archive/list`);
  });

  it('should get archive with id', () => {
    planningService.getArchive(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/archive/1`);
  });

  it('should import data into planning with archive id 1', () => {
    planningService.importIntoPlanning(1).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/import/archive/1`);
  });

  it('should search for plans', () => {
    planningService.searchCurrentStandby(new SearchCurrentStandbyObject()).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/searchnow`);
  });

  /**
   * Validation
   */

  it('should validate plans', () => {
    planningService.validate(1, new SchedulePlanningObject()).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/validation/standbyschedule/status/1`);
  });

  /**
   * Deletion
   */
  it('should delete plans', () => {
    planningService.deletePlan(1, new SearchBodiesObject()).subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/standbyschedule/delete/bodies/1`);
  });
});

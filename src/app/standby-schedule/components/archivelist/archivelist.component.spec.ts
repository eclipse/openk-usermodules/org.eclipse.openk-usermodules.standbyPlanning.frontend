/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';

import { of } from 'rxjs';
import { MessageService } from 'primeng/components/common/messageservice';

import { ArchivelistComponent } from './archivelist.component';
import { SharedModule } from '@shared/shared.module';
import { ArchiveObject } from '@shared/model/ArchiveObject';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { PlanningService } from '@schedule/services/planning.service';

export class PlanningServiceMock {
  getArchiveData() {
    return of([new ArchiveObject()]);
  }
  getArchive() {
    return of(new ArchiveObject());
  }
}

describe('ArchivelistComponent', () => {
  let component: ArchivelistComponent;
  let fixture: ComponentFixture<ArchivelistComponent>;
  const routes: Routes = [
    {
      path: '**',
      component: AlertComponent
    }
  ];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ArchivelistComponent],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        MessageService,
        {
          provide: PlanningService,
          useClass: PlanningServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchivelistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should navigate to archive entry', () => {
    const event = { data: { id: 1 } };
    component.rowClicked(event);
  });

  describe('search()', () => {
    it('should search if form is valid', () => {
      component.form.patchValue({
        date: { dateIndex: '2018-01-01' }
      });
      component.search();
    });
    it('shouldn´t search if form is invalid', () => {
      component.form.patchValue({
        date: { dateIndex: '' },
        city: ''
      });
      component.search();
    });
  });

  describe('clear()', () => {
    it('should clear the form', () => {
      component.form.patchValue({
        date: {
          dateIndex: '2018-01-01'
        }
      });
      component.clear();
      expect(component.form.getRawValue().date.dateIndex).toBe(null);
    });
  });
});

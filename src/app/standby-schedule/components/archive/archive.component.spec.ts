/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Router, Routes } from '@angular/router';

import { MessageService } from 'primeng/components/common/messageservice';
import { of } from 'rxjs';

import { ArchiveComponent } from './archive.component';
import { SharedModule } from '@shared/shared.module';
import { PlanningService } from '@schedule/services/planning.service';
import { ArchiveObject } from '@shared/model/ArchiveObject';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ProtocolObject } from '@shared/model/ProtocolObject';
import { StandbyScheduleModule } from '@schedule/standby-schedule.module';

export class PlanningServiceMock {
  getArchive() {
    const archiveObject = new ArchiveObject();
    archiveObject.jsonPlan = '{}';
    return of(archiveObject);
  }

  importIntoPlanning() {
    return of(new ProtocolObject());
  }
}
describe('ArchiveComponent', () => {
  let component: ArchiveComponent;
  let fixture: ComponentFixture<ArchiveComponent>;
  const routes: Routes = [
    {
      path: '**',
      component: AlertComponent
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes),
        StandbyScheduleModule
      ],
      providers: [
        {
          provide: PlanningService,
          useClass: PlanningServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('importIntoPlanning()', () => {
    it('should import into planning', () => {
      component.importIntoPlanning();
      component.archiveModalWarningRef.componentInstance.decision.next(true);
      expect(component).toBeTruthy();
    });

    it('shouldn´t import into planning', () => {
      component.importIntoPlanning();
      component.archiveModalWarningRef.componentInstance.decision.next(false);
      expect(component).toBeTruthy();
    });
  });
});

/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessageService } from 'primeng/components/common/messageservice';
import { of } from 'rxjs';

import { PlanninglistComponent } from './planninglist.component';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { StandbyScheduleModule } from '@schedule/standby-schedule.module';
import { Routes } from '@angular/router';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { PlanningService } from '@schedule/services/planning.service';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { GroupBodyObject } from '@shared/model/GroupBodyObject';
import { SearchBodiesObject } from '@shared/model/SearchBodiesObject';
import { PlanBodyObject } from '@shared/model/PlanBodyObject';
import { ReplaceUserObject, MoveUserObject } from '@shared/model/PlanActionsObject';
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { UserObject } from '@shared/model/UserObject';
import { PlanninglistMockObjects } from '@shared/testing/planninglist';

const planninglistMockObjects = new PlanninglistMockObjects;

export class MasterDataServiceMock {
  getStandbyListDropdown() {
    return of([planninglistMockObjects.STANDBYLIST_SELECTION_ARRAY]);
  }
}

export class PlanningServiceMock {
  getPlanningData() {
    return of(planninglistMockObjects.SCHEDULE_PLANNING_ARRAY);
  }
  startCalculation() {
    return of([planninglistMockObjects.PROTOCOL_OBJECT]);
  }
  filterBodies() {
    return of(new PlanBodyObject());
  }
  archivePlan() {
    return of(true);
  }
  replaceUser(data) {
    if (data) {
      return of(new ReplaceUserObject());
    } else {
      return false;
    }
  }
  moveUser() {
    return of(new MoveUserObject());
  }
  validate() {
    return of([planninglistMockObjects.PROTOCOL_OBJECT]);
  }
  deletePlan() {
    return of();
  }
}

describe('PlanninglistComponent', () => {
  let component: PlanninglistComponent;
  let fixture: ComponentFixture<PlanninglistComponent>;

  beforeEach(async(() => {

  }));

  beforeEach(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes),
        StandbyScheduleModule // needed in order to have the CalculateDialogComponent for the openModal method
      ],
      providers: [
        PlanningService,
        MasterdataService,
        MessageService,
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: PlanningService,
          useClass: PlanningServiceMock
        }
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PlanninglistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('openCalculationModal', () => {
    it('should open a modal and start calculation if the action is "calculate"', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 },
        },
        statusId: 1
      });
      component.openCalculationModal(1);
      component.modalCalculationRef.componentInstance.modalAction.next({
        action: 'calculate', data: {
          date: {
            validFrom: { day: 1, month: 10, year: 2018 },
            validTo: { day: 2, month: 10, year: 2018 },
          }
        }
      });
    });
    it('shouldn´t start a calculation if the action is not "calculate"', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 },
        },
        statusId: 1
      });
      component.openCalculationModal(1);
      component.modalCalculationRef.componentInstance.modalAction.next({ action: 'close', data: {} });
    });
  });
  describe('search', () => {
    it('should search for bodies', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.prepareDataAndCall('search');
    });

    it('shouldn´t search for bodies as form is invalid', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: '',
          validTo: ''
        }
      });
      component.prepareDataAndCall('search');
    });
  });

  describe('archive', () => {
    it('should archive bodies', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.prepareDataAndCall('archive');
    });

    it('shouldn´t archive bodies as form is invalid', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: '',
          validTo: '',
        },
        statusId: 1
      });
      component.prepareDataAndCall('archive');
    });

    it('shouldn´t do anything if the keyword action is empty', () => {
      component.prepareDataAndCall('');
    });
  });

  describe('archiveBodies()', () => {
    it('should open archive modal and archive plan if decision is true and data is returned', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.prepareDataAndCall('archive');
      component.archiveModalRef.componentInstance.decision.next({ title: 'test', comment: 'test' });
    });

    it('should close the modal without any rest call if the data is undefined', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.prepareDataAndCall('archive');
      component.archiveModalRef.componentInstance.decision.next(undefined);
    });
  });

  describe('filterBodies', () => {
    it('should get bodies according to the data input', () => {
      component.filterBodies(new SearchBodiesObject());
    });
  });

  describe('openReplaceDialog', () => {
    it('should open a modal and filter bodies on decision', () => {
      component.planStatusId = 1;
      component.currentStandby = new GroupBodyObject();
      component.currentStandby.standbyGroup = new StandbygroupObject();
      component.currentStandby.standbyGroup.id = 1;
      component.currentStandby.user = new UserObject();
      component.currentStandby.user.id = 1;
      component.currentStandby.validFrom = '2018-10-01';
      component.currentStandby.validTo = '2018-10-02';
      component.openReplaceDialog();
      component.modalReplaceDialogRef.componentInstance.decision.next(true);
    });

    it('should open a modal and shouldn´t filter bodies on decision if its false', () => {
      component.planStatusId = 1;
      component.currentStandby = new GroupBodyObject();
      component.currentStandby.standbyGroup = new StandbygroupObject();
      component.currentStandby.standbyGroup.id = 1;
      component.currentStandby.user = new UserObject();
      component.currentStandby.user.id = 1;
      component.currentStandby.validFrom = '2018-10-01';
      component.currentStandby.validTo = '2018-10-02';
      component.openReplaceDialog();
      component.modalReplaceDialogRef.componentInstance.decision.next(false);
    });
  });

  describe('openMoveDialog', () => {
    it('should open a modal and filter bodies on decision', () => {
      component.planStatusId = 1;
      component.currentStandby = new GroupBodyObject();
      component.currentStandby.standbyGroup = new StandbygroupObject();
      component.currentStandby.standbyGroup.id = 1;
      component.currentStandby.user = new UserObject();
      component.currentStandby.user.id = 1;
      component.currentStandby.validFrom = '2018-10-01';
      component.currentStandby.validTo = '2018-10-02';
      component.plan = new PlanBodyObject();
      component.plan.planHeader = { label: '', listGroups: [] };
      component.openMoveDialog();
      component.modalMoveDialogRef.componentInstance.decision.next(true);
    });

    it('should open a modal and shouldn´t filter bodies on decision if its false', () => {
      component.planStatusId = 1;
      component.currentStandby = new GroupBodyObject();
      component.currentStandby.standbyGroup = new StandbygroupObject();
      component.currentStandby.standbyGroup.id = 1;
      component.currentStandby.user = new UserObject();
      component.currentStandby.user.id = 1;
      component.currentStandby.validFrom = '2018-10-01';
      component.currentStandby.validTo = '2018-10-02';
      component.plan = new PlanBodyObject();
      component.plan.planHeader = { label: '', listGroups: [] };
      component.openMoveDialog();
      component.modalMoveDialogRef.componentInstance.decision.next(false);
    });
  });

  describe('setDefaultDate', () => {
    it('should set validFrom to current day', () => {
      const date = new Date();
      component.setDefaultDate('validFrom');
      expect(component.form.get('date').get('validFrom').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });

    it('should set validTo to current day + 15 years', () => {
      const date = new Date();
      component.setDefaultDate('validTo');
      expect(component.form.get('date').get('validTo').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() + 15
      });
    });
  });

  describe('validate()', () => {
    it('should validate whole plan with all the groups in a standbylist', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.validatePlan(undefined);
    });

    it('should validate the given group', () => {
      component.form.patchValue({
        standbyListId: '',
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.validatePlan(1);
    });
  });

  describe('deletePlan()', () => {
    it('should import into planning', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.deletePlan();
      component.deleteModalWarningRef.componentInstance.decision.next(true);
      expect(component).toBeTruthy();
    });

    it('shouldn´t import into planning', () => {
      component.form.patchValue({
        standbyListId: 1,
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.deletePlan();
      component.deleteModalWarningRef.componentInstance.decision.next(false);
      expect(component).toBeTruthy();
    });

    it('should return false on validating the form', () => {
      component.form.patchValue({
        standbyListId: '',
        date: {
          validFrom: { day: 1, month: 10, year: 2018 },
          validTo: { day: 2, month: 10, year: 2018 }
        }
      });
      component.deletePlan();
    });
  });
});

/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { TransferplanComponent } from './transferplan.component';
import { SharedModule } from '@shared/shared.module';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { StandbyScheduleModule } from '@schedule/standby-schedule.module';
import { PlanningService } from '@schedule/services/planning.service';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { CopyBodiesObject } from '@shared/model/CopyBodiesObject';
import { ProtocolObject } from '@shared/model/ProtocolObject';
import { TransferplanMockObjects } from '@shared/testing/transferplan';

const transferplanMockObjects = new TransferplanMockObjects;

export class MasterDataServiceMock {
  getStandbygroupsAndLists() {
    return of(transferplanMockObjects.BODY_GROUP_ARRAY);
  }
}

export class PlanningServiceMock {
  transferBodies() {
    return of(new ProtocolObject());
  }
}

describe('TransferplanComponent', () => {
  let component: TransferplanComponent;
  let fixture: ComponentFixture<TransferplanComponent>;

  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes),
        StandbyScheduleModule // needed in order to have the dialog components for the openModal method
      ],
      providers: [
        PlanningService,
        MasterdataService,
        MessageService,
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: PlanningService,
          useClass: PlanningServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransferplanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('moveBodies()', () => {
    it('should moveBodies', () => {
      const dataToMove: CopyBodiesObject = {
        lsTransferGroup: transferplanMockObjects.BODY_GROUP_ARRAY,
        overwrite: false,
        statusId: 1,
        validFrom: '2018-10-02',
        validTo: '2018-10-02'
      };
      component.moveBodies(dataToMove, 1);
      component.bodyModalRef.componentInstance.decision.next({ overwrite: false, validFrom: '2018-12-12', validTo: '2018-12-14' });
      expect(component).toBeTruthy();
    });
    it('shouldn´t move bodies if no data is entered in modal', () => {
      const dataToMove: CopyBodiesObject = {
        lsTransferGroup: transferplanMockObjects.BODY_GROUP_ARRAY,
        overwrite: false,
        statusId: 1,
        validFrom: '2018-10-02',
        validTo: '2018-10-02'
      };
      component.moveBodies(dataToMove, 1);
      component.bodyModalRef.componentInstance.decision.next(undefined);
      expect(component).toBeTruthy();
    });
  });
});

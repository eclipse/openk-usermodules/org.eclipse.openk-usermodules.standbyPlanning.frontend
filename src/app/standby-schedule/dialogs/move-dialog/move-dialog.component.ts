/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, EventEmitter, Injector, Output, Input } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { FormUtil } from '@shared/utils/form.util';
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';

@Component({
  selector: 'ok-move-dialog',
  templateUrl: './move-dialog.component.html',
  styleUrls: ['./move-dialog.component.scss']
})
export class MoveDialogComponent extends AbstractDialogComponent implements OnInit {
  @Output()
  decision: EventEmitter<any> = new EventEmitter<any>();

  @Input()
  standbyGroupList: Array<StandbygroupObject>;

  form: FormGroup = this.fb.group({
    date: this.fb.group({
      validFrom: ['', Validators.required],
      validTo: ['', Validators.required],
    }, { validator: [DatepickerValidator.dateRangeTo('')] }),
    validFromTime: '',
    validToTime: '',
    standbyGroupId: ['', Validators.required],
    scheduleBodyId: '',
    newUserId: '',
    statusId: ''
  });
  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
  }

  decide(decision) {
    const dataToTransmit = this.form.getRawValue();
    FormUtil.formatDates(dataToTransmit, ['validFrom', 'validTo']);
    FormUtil.addTimeToDates(dataToTransmit);
    delete dataToTransmit.validFromTime;
    delete dataToTransmit.validToTime;
    super.decide(decision, dataToTransmit);
  }

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

}

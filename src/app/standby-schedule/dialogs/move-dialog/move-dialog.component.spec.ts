/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';

import { MessageService } from 'primeng/components/common/messageservice';

import { MoveDialogComponent } from './move-dialog.component';
import { SharedModule } from '@shared/shared.module';

describe('MoveDialogComponent', () => {
  let component: MoveDialogComponent;
  let fixture: ComponentFixture<MoveDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [MoveDialogComponent],
      imports: [
        SharedModule,
        RouterTestingModule
      ],
      providers: [
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MoveDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('decide()', () => {
    it('should prepare the data and call decide', () => {
      component.form.patchValue({
        date: {
          validFrom: { day: 1, month: 1, year: 2018 },
          validTo: { day: 1, month: 1, year: 2018 }
        }
      });
      component.decide(true);
    });
    it('should validate form fields if decision is true and form fields are invalid', () => {
      component.form.patchValue({
        date: {
          validFrom: '',
          validTo: ''
        }
      });
      component.decide(true);
      expect(component.form.valid).toBeFalsy();
    });
  });

  describe('setDefaultDate', () => {
    it('should set validFrom to current day', () => {
      const date = new Date();
      component.setDefaultDate('validFrom');
      expect(component.form.get('date').get('validFrom').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });

    it('should set validTo to current day + 15 years', () => {
      const date = new Date();
      component.setDefaultDate('validTo');
      expect(component.form.get('date').get('validTo').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear() + 15
      });
    });
  });
});

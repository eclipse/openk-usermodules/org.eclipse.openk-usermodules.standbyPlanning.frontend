/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, Injector } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { AbstractDialogComponent } from '@shared/abstract/abstract-dialog/abstract-dialog.component';

@Component({
  selector: 'ok-archive-dialog',
  templateUrl: './archive-dialog.component.html',
  styleUrls: ['./archive-dialog.component.scss']
})
export class ArchiveDialogComponent extends AbstractDialogComponent implements OnInit {
  form: FormGroup = this.fb.group({
    title: '',
    comment: ''
  });
  constructor(
    private fb: FormBuilder
  ) {
    super();
  }

  ngOnInit() {
  }

  decide(decision: boolean) {
    const dataToTransmit = this.form.getRawValue();
    super.decide(decision, dataToTransmit);
  }

}

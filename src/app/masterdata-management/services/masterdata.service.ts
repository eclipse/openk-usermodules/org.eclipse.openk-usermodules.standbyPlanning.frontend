/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { UtilService } from '@core/services/util.service';
import { UserObject } from '@shared/model/UserObject';
import { LocationObject } from '@shared/model/LocationObject';
import { RegionObject } from '@shared/model/RegionObject';
import { FunctionObject } from '@shared/model/FunctionObject';
import { PostcodeObject } from '@shared/model/PostcodeObject';
import { StandbygroupObject } from '@shared/model/StandbygroupObject';
import { RegionHasFunctionObject } from '@shared/model/RegionHasFunctionObject';
import { UserInStandbygroupObject } from '@shared/model/UserInStandbygroupObject';
import { StandbyDurationObject } from '@shared/model/StandbyDurationObject';
import { BranchObject } from '@shared/model/BranchObject';
import { OrganisationObject } from '@shared/model/OrganisationObject';
import { StandbylistObject } from '@shared/model/StandbylistObject';
import { DateObject } from '@shared/model/DateObject';
import { ProtocolObject } from '@shared/model/ProtocolObject';
import { CopyStandbygroupObject } from '@shared/model/CopyStandbygroupObject';
import { UserDropdownObject } from '@shared/model/UserDropdownObject';

@Injectable({
  providedIn: 'root'
})
export class MasterdataService {

  constructor(
    private http: HttpClient,
    private utilService: UtilService
  ) { }

  /**
   * User
   */
  getUserData() {
    return this.http.get<UserObject[]>(`${this.utilService.readConfig('basePath')}/user/list`);
  }

  getUserDataSelection() {
    return this.http.get<UserObject[]>(`${this.utilService.readConfig('basePath')}/user/selectionlist`);
  }

  getUser(id) {
    return this.http.get<UserObject>(`${this.utilService.readConfig('basePath')}/user/${id}`);
  }

  saveUser(userObj: UserObject) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/user/save`, userObj);
  }

  anonymizeUser(id: number) {
    return this.http.get(`${this.utilService.readConfig('basePath')}/pseudonymize/user/${id}/true`);
  }


  getUserInStandbygroup() {
    return this.http.get<UserInStandbygroupObject[]>(`${this.utilService.readConfig('basePath')}/standbygroupuser/list`);
  }

  addStandbyGroupUser(id: number, userObjArray: UserObject[]) {
    return this.http.put<ProtocolObject>(`${this.utilService.readConfig('basePath')}/standbygroup/${id}/user/save/list`, userObjArray);
  }

  deleteStandbyGroupUser(id: number, userObjArray: UserObject[]) {
    return this.http.put<ProtocolObject>(`${this.utilService.readConfig('basePath')}/standbygroup/${id}/user/delete/list`, userObjArray);
  }

  addUserRegion(id: number, regionObjArray: RegionObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/user/${id}/region/save/list`, regionObjArray);
  }

  deleteUserRegion(id: number, regionObjArray: RegionObject[]) {
    return this.http.put<RegionObject[]>(`${this.utilService.readConfig('basePath')}/user/${id}/region/delete/list`, regionObjArray);
  }

  addUserFunction(id: number, functionObjArray: FunctionObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/user/${id}/userfunction/save/list`, functionObjArray);
  }

  deleteUserFunction(id: number, functionObjArray: FunctionObject[]) {
    return this.http.put<FunctionObject[]>(
      `${this.utilService.readConfig('basePath')}/user/${id}/userfunction/delete/list`,
      functionObjArray
    );
  }

  filterUsersByStandbygroup(id) {
    return this.http.get<UserObject[]>(`${this.utilService.readConfig('basePath')}/user/filter/standbygroup/${id}`);
  }

  getUserlistDropdown() {
    return this.http.get<UserDropdownObject[]>(`${this.utilService.readConfig('basePath')}/user/dropdownlist`);
  }

  /**
   * Organisation
   */
  getOrganisationData() {
    return this.http.get<OrganisationObject[]>(`${this.utilService.readConfig('basePath')}/organisation/list`);
  }

  getOrganisationDataSelection() {
    return this.http.get<OrganisationObject[]>(`${this.utilService.readConfig('basePath')}/organisation/selectionlist`);
  }

  getOrganisation(id) {
    return this.http.get<OrganisationObject>(`${this.utilService.readConfig('basePath')}/organisation/${id}`);
  }

  saveOrganisation(organisationObj: OrganisationObject) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/organisation/save`, organisationObj);
  }

  /**
   * Location
   */

  getLocationData() {
    return this.http.get<LocationObject[]>(`${this.utilService.readConfig('basePath')}/location/list`);
  }

  getLocationDataSelection() {
    return this.http.get<LocationObject[]>(`${this.utilService.readConfig('basePath')}/location/selectionlist`);
  }

  saveLocation(locationObj: LocationObject) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/location/save`, locationObj);
  }

  getLocation(id) {
    return this.http.get<LocationObject>(`${this.utilService.readConfig('basePath')}/location/${id}`);
  }

  addLocationBranch(id: number, branchObjArray: BranchObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/location/${id}/branch/save/list`, branchObjArray);
  }

  deleteLocationBranch(id: number, branchObjArray: BranchObject[]) {
    return this.http.put<BranchObject[]>(`${this.utilService.readConfig('basePath')}/location/${id}/branch/delete/list`, branchObjArray);
  }

  addLocationRegion(id: number, regionObjArray: RegionObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/location/${id}/region/save/list`, regionObjArray);
  }

  deleteLocationRegion(id: number, regionObjArray: RegionObject[]) {
    return this.http.put<RegionObject[]>(`${this.utilService.readConfig('basePath')}/location/${id}/region/delete/list`, regionObjArray);
  }

  addPostcodes(id: number, postcodeObjArray: PostcodeObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/location/${id}/postcode/save/list`, postcodeObjArray);
  }

  deletePostcodes(id: number, postcodeObjArray: PostcodeObject[]) {
    return this.http.put<PostcodeObject[]>(
      `${this.utilService.readConfig('basePath')}/location/${id}/postcode/delete/list`,
      postcodeObjArray
    );
  }

  /**
   * Region
   */

  getRegionData() {
    return this.http.get<RegionObject[]>(`${this.utilService.readConfig('basePath')}/region/list`);
  }

  getRegionDataSelection() {
    return this.http.get<RegionObject[]>(`${this.utilService.readConfig('basePath')}/region/selectionlist`);
  }

  getRegionHasFunction() {
    return this.http.get<RegionHasFunctionObject[]>(`${this.utilService.readConfig('basePath')}/regionhasfunction/list`);
  }

  saveRegion(regionObj: RegionObject) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/region/save`, regionObj);
  }

  getRegion(id) {
    return this.http.get<RegionObject>(`${this.utilService.readConfig('basePath')}/region/${id}`);
  }

  addStandbyGroupRegion(id: number, regionObjArray: RegionObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/standbygroup/${id}/region/save/list`, regionObjArray);
  }

  deleteStandbyGroupRegion(id: number, regionObjArray: RegionObject[]) {
    return this.http.put<RegionObject[]>(
      `${this.utilService.readConfig('basePath')}/standbygroup/${id}/region/delete/list`,
      regionObjArray
    );
  }

  addRegionLocation(id: number, locationObjArray: LocationObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/region/${id}/location/save/list`, locationObjArray);
  }

  deleteRegionLocation(id: number, locationObjArray: LocationObject[]) {
    return this.http.put<LocationObject[]>(
      `${this.utilService.readConfig('basePath')}/region/${id}/location/delete/list`,
      locationObjArray
    );
  }

  addRegionFunction(id: number, functionObjArray: FunctionObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/region/${id}/userfunction/save/list`, functionObjArray);
  }

  deleteRegionFunction(id: number, functionObjArray: FunctionObject[]) {
    return this.http.put<FunctionObject[]>(
      `${this.utilService.readConfig('basePath')}/region/${id}/userfunction/delete/list`,
      functionObjArray
    );
  }

  /**
   * Function
   */

  getFunctionData() {
    return this.http.get<FunctionObject[]>(`${this.utilService.readConfig('basePath')}/userfunction/list`);
  }

  getFunctionDataSelection() {
    return this.http.get<FunctionObject[]>(`${this.utilService.readConfig('basePath')}/userfunction/selectionlist`);
  }

  saveFunction(functionObj: FunctionObject) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/userfunction/save`, functionObj);
  }

  getFunction(id) {
    return this.http.get<FunctionObject>(`${this.utilService.readConfig('basePath')}/userfunction/${id}`);
  }

  /**
   * Postcodes
   */

  getPostcodeData() {
    return this.http.get<PostcodeObject[]>(`${this.utilService.readConfig('basePath')}/postcode/list`);
  }

  /**
   * Standbygroups
   */

  getStandbygroupData() {
    return this.http.get<StandbygroupObject[]>(`${this.utilService.readConfig('basePath')}/standbygroup/list`);
  }

  getStandbygroupSelection() {
    return this.http.get<StandbygroupObject[]>(`${this.utilService.readConfig('basePath')}/standbygroup/selectionlist`);
  }

  saveStandbygroup(standbygroupObj: StandbygroupObject) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/standbygroup/save`, standbygroupObj);
  }

  getStandbygroup(id) {
    return this.http.get<StandbygroupObject>(`${this.utilService.readConfig('basePath')}/standbygroup/${id}`);
  }

  getStandbygroupUniqueUser(id) {
    return this.http.get<UserObject[]>(`${this.utilService.readConfig('basePath')}/standbygroup/${id}/user/list/unique`);
  }

  addStandbygroupFunction(id: number, functionObjArray: FunctionObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/standbygroup/${id}/userfunction/save/list`, functionObjArray);
  }

  deleteStandbygroupFunction(id: number, functionObjArray: FunctionObject[]) {
    return this.http.put<FunctionObject[]>(
      `${this.utilService.readConfig('basePath')}/standbygroup/${id}/userfunction/delete/list`,
      functionObjArray
    );
  }

  addStandbygroupCalendar(id: number, calendarObjArray: DateObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/standbygroup/${id}/calendar/ignore/save/list`, calendarObjArray);
  }

  deleteStandbygroupCalendar(id: number, calendarObjArray: DateObject[]) {
    return this.http.put<DateObject[]>(
      `${this.utilService.readConfig('basePath')}/standbygroup/${id}/calendar/ignore/delete/list`,
      calendarObjArray
    );
  }

  getStandbygroupsAndLists() {
    return this.http.get<StandbygroupObject[]>(`${this.utilService.readConfig('basePath')}/standbygroup/transfer/list`);
  }

  copyStandbygroup(id: number, copyObject: CopyStandbygroupObject) {
    return this.http.put<StandbygroupObject>(
      `${this.utilService.readConfig('basePath')}/standbygroup/${id}/copy`,
      copyObject
    );
  }

  /**
   * Standbygroupduration
   */

  saveStandbygroupDuration(id: number, standbygroupObj) {
    return this.http.put<ProtocolObject>(
      `${this.utilService.readConfig('basePath')}/standbygroup/${id}/duration/save/list`,
      standbygroupObj
    );
  }

  deleteStandbygroupDuration(id, standbydurationObjArray: StandbyDurationObject[]) {
    return this.http.put<StandbyDurationObject[]>(
      `${this.utilService.readConfig('basePath')}/standbygroup/${id}/duration/delete/list`, standbydurationObjArray
    );
  }

  /**
   * Branches
   */

  getBranchDataSelection() {
    return this.http.get<BranchObject[]>(`${this.utilService.readConfig('basePath')}/branch/selectionlist`);
  }

  addStandbyGroupBranch(id: number, branchObjArray: BranchObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/standbygroup/${id}/branch/save/list`, branchObjArray);
  }

  deleteStandbyGroupBranch(id: number, branchObjArray: BranchObject[]) {
    return this.http.put<BranchObject[]>(
      `${this.utilService.readConfig('basePath')}/standbygroup/${id}/branch/delete/list`, branchObjArray
    );
  }

  /**
   * Standbylists
   */

  saveStandbylist(standbylistObj: StandbylistObject) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/standbylist/save`, standbylistObj);
  }

  getStandbylist(id) {
    return this.http.get<StandbylistObject>(`${this.utilService.readConfig('basePath')}/standbylist/${id}`);
  }

  getStandbyListData() {
    return this.http.get<StandbylistObject[]>(`${this.utilService.readConfig('basePath')}/standbylist/list`);
  }

  getStandbyListSelection() {
    return this.http.get<StandbylistObject[]>(`${this.utilService.readConfig('basePath')}/standbylist/selectionlist`);
  }

  getStandbyListDropdown() {
    return this.http.get<StandbylistObject[]>(`${this.utilService.readConfig('basePath')}/standbylist/dropdownlist`);
  }

  addStandbylistStandbygroup(id: number, branchObjArray: BranchObject[]) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/standbylist/${id}/standbygroup/save/list`, branchObjArray);
  }

  deleteStandbylistStandbygroup(id: number, branchObjArray: BranchObject[]) {
    return this.http.put<BranchObject[]>(
      `${this.utilService.readConfig('basePath')}/standbylist/${id}/standbygroup/delete/list`, branchObjArray
    );
  }

  /**
   * Calendar
   */

  saveDate(dateObj: DateObject) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/calendar/save`, dateObj);
  }

  deleteDate(dateObj) {
    return this.http.put(`${this.utilService.readConfig('basePath')}/calendar/delete`, dateObj);
  }

  getDate(id) {
    return this.http.get<DateObject>(`${this.utilService.readConfig('basePath')}/calendar/${id}`);
  }

  getCalendarData() {
    return this.http.get<DateObject[]>(`${this.utilService.readConfig('basePath')}/calendar/list`);
  }
}

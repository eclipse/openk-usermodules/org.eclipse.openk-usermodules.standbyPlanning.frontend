/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { Subject, Subscription } from 'rxjs';
import { Params } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { FunctionObject } from '@shared/model/FunctionObject';
import { FormUtil } from '@shared/utils/form.util';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-function',
  templateUrl: './function.component.html',
  styleUrls: ['./function.component.scss']
})
export class FunctionComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  decisionModalRef: NgbModalRef;
  decision = new Subject<boolean>();

  param$: Subscription;
  function$: Subscription;
  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    this.param$ = this.route.params.subscribe((params: Params) => {
      const id = params['id'];
      if (id && id !== 'new') {
        this.function$ = this.masterDataService.getFunction(id).subscribe(
          data => {
            this.form.patchValue(data);
          }
        );
      }
    });
  }

  createForm() {
    this.form = this.fb.group({
      functionId: '',
      functionName: ['', Validators.required]
    });
  }
  /**
   * Save and Validation Methods
   */

  /**
   * Saves the current Form
   */
  saveFunction() {
    if (FormUtil.validate(this.form)) {
      const functionToSave = this.form.getRawValue();
      this.masterDataService.saveFunction(functionToSave).subscribe((res: FunctionObject) => {
        // neccessary due to canDeactivate guard
        this.form.markAsPristine();
        this.form.markAsUntouched();
        this.router.navigate(['/stammdatenverwaltung/funktion']);
      });
      return true;
    }
    return false;
  }

  ngOnDestroy() {
    if (this.param$) {
      this.param$.unsubscribe();
    }
    if (this.function$) {
      this.function$.unsubscribe();
    }
  }
}

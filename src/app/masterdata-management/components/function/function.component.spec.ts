/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ActivatedRoute, Routes } from '@angular/router';

import { SharedModule } from '@shared/shared.module';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { FunctionMockObjects } from '@shared/testing/function';
import { FunctionComponent } from '@masterdata/components/function/function.component';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { MessageService } from 'primeng/components/common/messageservice';

const functionMockObjects = new FunctionMockObjects;

export class MasterDataServiceMock {
  saveFunction() {
    return of(functionMockObjects.FUNCTION_OBJECT);
  } // or whatever dummy state value you want to use
  getFunction() {
    return of(functionMockObjects.FUNCTION_OBJECT);
  }
}

describe('FunctionComponent', () => {
  let component: FunctionComponent;
  let fixture: ComponentFixture<FunctionComponent>;


  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [
        FunctionComponent
      ],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('validateAllFormFields', () => {
    it('should not save the form if there are validation errors', () => {
      expect(component.saveFunction()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      component.form.patchValue({ functionName: 'test' });
      expect(component.saveFunction()).toBeTruthy();
    });
  });
});

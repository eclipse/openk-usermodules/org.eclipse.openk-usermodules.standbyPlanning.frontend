/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { ActivatedRoute, Routes, Router } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { SharedModule } from '@shared/shared.module';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { StandbylistMockObjects } from '@shared/testing/standbylist';
import { StandbylistComponent } from '@masterdata/components/standbylist/standbylist.component';
import { MessageService } from 'primeng/components/common/messageservice';
import { MasterdataManagementModule } from '@masterdata/masterdata-management.module';

const standbygroupMockObjects = new StandbylistMockObjects;

export class MasterDataServiceMock {
  saveStandbylist() {
    return of(standbygroupMockObjects.STANDBYLIST_OBJECT);
  }
  getStandbylist() {
    return of(standbygroupMockObjects.STANDBYLIST_OBJECT);
  }
  getStandbygroupSelection() {
    return of(standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY);
  }
  addStandbylistStandbygroup() {
    return of(standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY);
  }
  deleteStandbylistStandbygroup() {
    return of(standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY);
  }
}

describe('StandbylistComponent', () => {
  let component: StandbylistComponent;
  let fixture: ComponentFixture<StandbylistComponent>;
  const router = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        SharedModule,
        MasterdataManagementModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        },
        {
          provide: ActivatedRoute,
          useValue: {
            params: of({ id: 123 })
          }
        },
        { provide: Router, useValue: router },
        MessageService
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandbylistComponent);
    component = fixture.componentInstance;
    component.sourceColumnDefsStandbygroup = [];
    component.targetColumnDefsStandbygroup = [];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('validateAllFormFields', () => {
    it('should not save the form if there are validation errors', () => {
      component.form.patchValue({ title: '' });
      expect(component.saveStandbylist()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      component.form.patchValue({ title: 'test' });
      expect(component.saveStandbylist()).toBeTruthy();
    });
  });
  describe('close()', () => {
    it('should navigate away when the component is not a modal', () => {
      component.isModal = false;
      component.close();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/bereitschaftsliste']);
    });

    it('should dispatch an event with content ´close´ if it is a modal and close() is called', () => {
      component.isModal = true;
      component.modalAction.subscribe((res) => {
        expect(res).toBe('close');
      });
      component.close();
    });
  });

  describe('saveStandbyGroup()', () => {
    it('should not save the form if there are validation errors', () => {
      expect(component.saveStandbylist()).toBeFalsy();
    });

    it('should save the form if there are no validation errors', () => {
      component.form.patchValue({ title: 'test' });
      expect(component.saveStandbylist()).toBeTruthy();
    });

    it('should save the form if the component is opened in a modal and navigate away', () => {
      component.form.patchValue({ title: 'test' });
      component.isModal = true;
      component.saveStandbylist();
      expect(router.navigate).toHaveBeenCalledWith(['/stammdatenverwaltung/bereitschaftsliste', 1]);
    });
  });

  describe('move functions standbygroups ', () => {
    it('should move standbygroups from source to target', () => {
      component.targetStandbygroup = standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY;
      component.moveToSourceStandbygroup(standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY);
      expect(component.targetStandbygroup.length).toBeGreaterThan(0);
    });

    it('should move standbygroups from target to source', () => {
      component.sourceStandbygroup = standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY;
      component.moveToTargetStandbygroup(standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY, false);
      expect(component.sourceStandbygroup.length).toBe(3);
    });
  });

  describe('change data of standbygroups ', () => {
    it('should change standbygroups if only one standbygroup is selected', () => {
      component.targetStandbygroup = standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY;
      component.changeTargetDataStandbygroup(standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY_ONE_ELEMENT);
      component.standbygroupModalRef.componentInstance.decision.next(
        standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY_ONE_ELEMENT
      );
      expect(component.targetStandbygroup.length).toBeGreaterThan(0);
    });

    it('shouldn´t change standbygroups if only one standbygroup is selected but user clicks cancel in dialog', () => {
      component.targetStandbygroup = standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY;
      component.changeTargetDataStandbygroup(standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY_ONE_ELEMENT);
      component.standbygroupModalRef.componentInstance.decision.next(undefined);
      expect(component.targetStandbygroup.length).toBeGreaterThan(0);
    });

    it('shouldn´t change standbygroups if multiple standbygroups are selected', () => {
      component.sourceStandbygroup = standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY;
      component.changeTargetDataStandbygroup(standbygroupMockObjects.STANDBYGROUPSELECTION_OBJECT_ARRAY);
      expect(component.sourceStandbygroup.length).toBe(3);
    });
  });
});

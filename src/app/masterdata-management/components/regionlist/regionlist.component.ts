/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { RegionObject } from '@shared/model/RegionObject';
import { ListUtil, AGGRID_LOCALETEXT, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { RegionComponent } from '@masterdata/components/region/region.component';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-regionlist',
  templateUrl: './regionlist.component.html',
  styleUrls: ['./regionlist.component.scss']
})
export class RegionlistComponent implements OnInit, OnDestroy {
  defaultColDef = DEFAULT_COL_DEF;
  columnDefs = [
    { headerName: 'Regionsname', field: 'regionName' },
    {
      headerName: 'Lokationen', field: 'lsLocations',
      cellRenderer: (params) => {
        return ListUtil.joinArrayElementsToString(params.value, 'community');
      }
    }
  ];
  localeText = AGGRID_LOCALETEXT;

  rowData = [];
  regionData$: Subscription;
  modalRef: NgbModalRef;

  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

  }

  onGridReady(params) {
    this.regionData$ = this.masterDataService.getRegionData().subscribe((res: RegionObject[]) => {
      this.rowData = res;
    });
    params.api.sizeColumnsToFit();
  }

  rowClicked(event) {
    this.router.navigate(['stammdatenverwaltung/region', event.data.id]);
  }

  ngOnDestroy() {
    if (this.regionData$) {
      this.regionData$.unsubscribe();
    }
  }

  openModal() {
    this.modalRef = this.modalService.open(RegionComponent, { size: 'lg', backdrop: 'static' });
    this.modalRef.componentInstance.isModal = true;
    const modalAction: Subscription = this.modalRef.componentInstance.modalAction.subscribe((res) => {
      if (res === 'close') {
        this.modalRef.close();
        modalAction.unsubscribe();
      }
    });
  }

}

/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { SharedModule } from '@shared/shared.module';
import { CalendarMockObjects } from '@shared/testing/calendar';
import { Routes } from '@angular/router';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { CalendarlistComponent } from '@masterdata/components/calendarlist/calendarlist.component';
import { MessageService } from 'primeng/components/common/messageservice';

const calendarMockObjects = new CalendarMockObjects;

export class MasterDataServiceMock {
  getCalendarData() {
    return of(calendarMockObjects.CALENDAR_ARRAY);
  }
}

describe('CalendarlistComponent', () => {
  let component: CalendarlistComponent;
  let fixture: ComponentFixture<CalendarlistComponent>;

  const routes: Routes = [
    {
      path: '**',
      component: AlertComponent
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [CalendarlistComponent],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        MasterdataService,
        MessageService,
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalendarlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onGridReady without error', () => {
    const params = {
      api: {
        sizeColumnsToFit: () => { }
      }
    };
    try {
      component.onGridReady(params);
    } catch (e) {
      expect(e).toBeFalsy();
    }
  });

  it('should navigate to a details view on rowClicked()', () => {
    const event = { data: { id: 1 } };
    component.rowClicked(event);
  });
});

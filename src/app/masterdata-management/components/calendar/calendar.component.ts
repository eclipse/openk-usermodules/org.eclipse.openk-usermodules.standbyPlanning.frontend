/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { Params } from '@angular/router';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { FormUtil } from '@shared/utils/form.util';
import { DateObject } from '@shared/model/DateObject';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  instanceId;

  param$: Subscription;
  date$: Subscription;
  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    this.param$ = this.route.params.subscribe((params: Params) => {
      this.instanceId = params['id'];
      if (this.instanceId && this.instanceId !== 'new') {
        this.date$ = this.masterDataService.getDate(this.instanceId).subscribe(
          data => {
            data.date = {
              dateIndex: this.ngbDateParserFormatter.parse(data.dateIndex)
            };
            this.form.patchValue(data);
          }
        );
      }
    });
  }

  createForm() {
    this.form = this.fb.group({
      id: '',
      name: ['', Validators.required],
      date: this.fb.group({
        dateIndex: ['', Validators.required]
      })
    });
  }
  /**
   * Save and Validation Methods
   */

  /**
   * Saves the current Form
   */
  saveDate() {
    if (FormUtil.validate(this.form)) {
      const dateToSave = this.form.getRawValue();
      FormUtil.formatDates(dateToSave, ['dateIndex']);
      this.masterDataService.saveDate(dateToSave).subscribe((res: DateObject) => {
        // neccessary due to canDeactivate guard
        this.form.markAsPristine();
        this.form.markAsUntouched();
        this.router.navigate(['/stammdatenverwaltung/kalender']);
      });
      return true;
    }
    return false;
  }

  deleteDate() {
    const dateToDelete = { id: this.form.getRawValue().id };
    this.masterDataService.deleteDate(dateToDelete).subscribe(() => {
      this.router.navigate(['/stammdatenverwaltung/kalender']);
    });
  }

  /**
   * Sets date field to today
   */

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

  ngOnDestroy() {
    if (this.param$) {
      this.param$.unsubscribe();
    }
    if (this.date$) {
      this.date$.unsubscribe();
    }
  }

}

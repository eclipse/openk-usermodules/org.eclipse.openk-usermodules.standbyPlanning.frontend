/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { OrganisationlistComponent } from '@masterdata/components/organisationlist/organisationlist.component';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { SharedModule } from '@shared/shared.module';
import { OrganisationMockObjects } from '@shared/testing/organisation';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { Routes } from '@angular/router';
import { MessageService } from 'primeng/components/common/messageservice';

const organisationMockObjects = new OrganisationMockObjects;

export class MasterDataServiceMock {
  getOrganisationData() {
    return of(organisationMockObjects.ORGANISATION_ARRAY);
  }
}

describe('OrganisationlistComponent', () => {
  let component: OrganisationlistComponent;
  let fixture: ComponentFixture<OrganisationlistComponent>;

  beforeEach(async(() => {
    const routes: Routes = [
      {
        path: '**',
        component: AlertComponent
      }
    ];
    TestBed.configureTestingModule({
      declarations: [OrganisationlistComponent],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        MasterdataService,
        MessageService,
        {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call onGridReady without error', () => {
    const params = {
      api: {
        sizeColumnsToFit: () => { }
      }
    };
    try {
      component.onGridReady(params);
    } catch (e) {
      expect(e).toBeFalsy();
    }
  });

  it('should navigate to a details view on rowClicked()', () => {
    const event = { data: { id: 1 } };
    component.rowClicked(event);
  });

});

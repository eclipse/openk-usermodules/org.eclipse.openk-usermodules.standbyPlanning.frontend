/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { Router } from '@angular/router';

import { NgbModalRef, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { MasterdataService } from '@masterdata/services/masterdata.service';
import { UserObject } from '@shared/model/UserObject';
import { ListUtil, AGGRID_LOCALETEXT, DEFAULT_COL_DEF } from '@shared/utils/list.util';
import { UserComponent } from '@masterdata/components/user/user.component';
import { AuthenticationService } from '@core/services/authentication.service';

@Component({
  selector: 'ok-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.scss']
})
export class UserlistComponent implements OnInit, OnDestroy {
  defaultColDef = DEFAULT_COL_DEF;
  columnDefs = [
    { headerName: 'Vorname', field: 'firstname' },
    { headerName: 'Nachname', field: 'lastname' },
    {
      headerName: 'Gültig von', field: 'validFrom', filter: 'agDateColumnFilter', valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    },
    {
      headerName: 'Gültig bis', field: 'validTo', filter: 'agDateColumnFilter', valueFormatter: ListUtil.formatDate,
      filterParams: {
        comparator: ListUtil.compareDates
      }
    },
    {
      headerName: 'Organisation',
      children: [
        { headerName: 'Organisationsname', field: 'organisation.orgaName' },
        { headerName: 'PLZ', field: 'organisation.address.postcode' },
        { headerName: 'Ort', field: 'organisation.address.community' },
        { headerName: 'Lokation', field: 'organisation.address.communitySuffix' },
        { headerName: 'Straße', field: 'organisation.address.street' },
        { headerName: 'Hausnummer', field: 'organisation.address.housenumber' },
      ]
    }
  ];

  localeText = AGGRID_LOCALETEXT;

  rowData: UserObject[] = [];
  userData$: Subscription;
  modalRef: NgbModalRef;

  constructor(
    public authService: AuthenticationService,
    private masterDataService: MasterdataService,
    private router: Router,
    private modalService: NgbModal
  ) { }

  ngOnInit() {

  }

  onGridReady(params) {
    this.userData$ = this.masterDataService.getUserData().subscribe((res: UserObject[]) => {
      this.rowData = res;
    });
    params.api.sizeColumnsToFit();
  }

  rowClicked(event) {
    this.router.navigate(['stammdatenverwaltung/mitarbeiter', event.data.id]);
  }

  ngOnDestroy() {
    if (this.userData$) {
      this.userData$.unsubscribe();
    }
  }

  openModal() {
    this.modalRef = this.modalService.open(UserComponent, { size: 'lg', backdrop: 'static' });
    this.modalRef.componentInstance.isModal = true;
    const modalAction: Subscription = this.modalRef.componentInstance.modalAction.subscribe((res) => {
      if (res === 'close') {
        this.modalRef.close();
        modalAction.unsubscribe();
      }
    });
  }

}

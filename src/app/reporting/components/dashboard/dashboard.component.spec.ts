/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Routes } from '@angular/router';
import { of } from 'rxjs';

import { MessageService } from 'primeng/components/common/messageservice';

import { DashboardComponent } from './dashboard.component';
import { SharedModule } from '@shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { AlertComponent } from '@shared/components/alert/alert.component';
import { PlanningService } from '@schedule/services/planning.service';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { ReportingService } from '@reporting/services/reporting.service';
import { StandbySearchList } from '@shared/model/StandbySearchList';
import { ReportingMockObjects } from '@shared/testing/reporting';
import { PlanninglistMockObjects } from '@shared/testing/planninglist';

const reportingMockObjects = new ReportingMockObjects();
const planninglistMockObjects = new PlanninglistMockObjects();

export class PlanningServiceMock {
  searchCurrentStandby() {
    return of(reportingMockObjects.QUERY_BODIES_OBJECT);
  }
}

export class MasterDataServiceMock {
  getLocation(data) {
    if (data === 1) {
      return of(reportingMockObjects.LOCATION_OBJECT_WITHOUT_BRANCHES);
    } else {
      return of(reportingMockObjects.LOCATION_OBJECT_WITH_BRANCHES);
    }
  }

  getStandbyListDropdown() {
    return of([planninglistMockObjects.STANDBYLIST_SELECTION_ARRAY]);
  }
}

export class ReportingServiceMock {
  getSearchList() {
    return of([new StandbySearchList()]);
  }
}

describe('DashboardComponent', () => {
  let component: DashboardComponent;
  let fixture: ComponentFixture<DashboardComponent>;
  const routes: Routes = [
    {
      path: '**',
      component: AlertComponent
    }
  ];
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DashboardComponent],
      imports: [
        SharedModule,
        RouterTestingModule.withRoutes(routes)
      ],
      providers: [
        MessageService,
        {
          provide: PlanningService,
          useClass: PlanningServiceMock
        }, {
          provide: MasterdataService,
          useClass: MasterDataServiceMock
        }, {
          provide: ReportingService,
          useClass: ReportingServiceMock
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('should get location if value changes', () => {
    it('should get locationData if value of locationId changes and print "Gas"', () => {
      component.form.patchValue({ locationId: 2 });
      expect(component.branches).toEqual([{ label: 'Gas', value: { id: 1, title: 'Gas' } }]);
    });
  });

  describe('setDefaultDate', () => {
    it('should set validFrom to current day', () => {
      const date = new Date();
      component.setDefaultDate('dateIndex');
      expect(component.form.get('date').get('dateIndex').value).toEqual({
        day: date.getDate(), month: date.getMonth() + 1, year: date.getFullYear()
      });
    });
  });
  describe('search()', () => {
    it('should search if form is valid', () => {
      component.form.patchValue({ dateIndex: '2018-01-01', city: 'Kassel' });
      component.search();
    });
    it('shouldn´t search if form is invalid', () => {
      component.form.patchValue({ dateIndex: '', city: '' });
      component.search();
    });
  });
});

/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';

import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { PlanningService } from '@schedule/services/planning.service';
import { FormUtil } from '@shared/utils/form.util';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { ReportingService } from '@reporting/services/reporting.service';
import { StandbySearchList } from '@shared/model/StandbySearchList';
import { StandbySearchBranches } from '@shared/model/StandbySearchBranches';
import { StandbylistObject } from '@shared/model/StandbylistObject';

@Component({
  selector: 'ok-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  rowData: any;
  searchList: StandbySearchList[];
  branches: StandbySearchBranches[] = [];
  standbylistSelectionData: StandbylistObject[];
  intervalRef;

  constructor(
    private planningService: PlanningService,
    private reportingService: ReportingService,
    private masterDataService: MasterdataService,
    private injector: Injector
  ) {
    super(injector);
  }

  ngOnInit() {
    this.createForm();
    FormUtil.setDefaultDate(this.form, 'dateIndex');
    this.getSearchList();
    this.getStandbyListData();
    this.search();
    this.form.get('locationId').valueChanges.subscribe((data) => {
      if (data) {
        this.masterDataService.getLocation(data).subscribe((locationData) => {
          this.branches = this.convertToMultiSelect(locationData.lsLocationForBranches);
        });
      }
      this.search();
    });
  }

  createForm() {
    let now = new Date();
    this.form = this.fb.group({
      date: this.fb.group({
        dateIndex: ['', Validators.required],
      }),
      timeIndex: {
        hour: now.getHours(),
        minute: now.getMinutes(),
        second: now.getSeconds()
      },
      locationId: '',
      lsBranchSelected: [],
      listId: ''
    });

    this.intervalRef = setInterval(() => {
      now = new Date();
      this.form.patchValue({
        timeIndex:
        {
          hour: now.getHours(),
          minute: now.getMinutes(),
          second: now.getSeconds()
        }
      });
    }, 1000 * 60);
  }

  convertToMultiSelect(branches): StandbySearchBranches[] {
    return branches.map(elem => {
      return {
        label: elem.title,
        value: { id: elem.branchId, title: elem.title }
      };
    });
  }

  search() {
    if (FormUtil.validate(this.form)) {
      const formValue = this.form.getRawValue();
      FormUtil.formatDates(formValue, ['dateIndex']);
      this.planningService.searchCurrentStandby(formValue).subscribe((searchRes) => {
        this.rowData = searchRes;
      });
    }
  }

  getSearchList() {
    this.reportingService.getSearchList().subscribe((searchData) => {
      this.searchList = searchData;
    });
  }

  getStandbyListData() {
    this.masterDataService.getStandbyListDropdown().subscribe((standbylistRes: StandbylistObject[]) => {
      this.standbylistSelectionData = standbylistRes;
    });
  }

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

  ngOnDestroy() {
    clearInterval(this.intervalRef);
  }
}

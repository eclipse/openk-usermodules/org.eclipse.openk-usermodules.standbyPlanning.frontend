/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { Component, OnInit, Injector, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { saveAs } from 'file-saver';

import { StandbylistObject } from '@shared/model/StandbylistObject';
import { AbstractFormComponent } from '@shared/abstract/abstract-form/abstract-form.component';
import { MasterdataService } from '@masterdata/services/masterdata.service';
import { ReportingService } from '@reporting/services/reporting.service';
import { ReportObject } from '@shared/model/ReportObject';
import { FormUtil } from '@shared/utils/form.util';
import { DatepickerValidator } from '@shared/validators/datepicker.validator';
import { AuthenticationService } from '@core/services/authentication.service';
import { UserDropdownObject } from '@shared/model/UserDropdownObject';

@Component({
  selector: 'ok-standby-reporting',
  templateUrl: './standby-reporting.component.html',
  styleUrls: ['./standby-reporting.component.scss']
})
export class StandbyReportingComponent extends AbstractFormComponent implements OnInit, OnDestroy {
  standbylistSelectionData = [];
  userSelectionData = [];
  reportSelectionData = [];

  isSearchingPerson = false;
  standbyListData$: Subscription;
  userData$: Subscription;
  reportData$: Subscription;

  constructor(
    private masterDataService: MasterdataService,
    private reportingService: ReportingService,
    private injector: Injector,
    public authService: AuthenticationService) {
    super(injector);
  }

  createForm() {
    this.form = this.fb.group({
      standById: ['', Validators.required],
      reportName: ['', Validators.required],
      date: this.fb.group({
        validFrom: ['', Validators.required],
        validTo: ['', Validators.required],
      }, { validator: [DatepickerValidator.dateRangeTo('')] }),
      validFromTime: { hour: 12, minute: 0 },
      validToTime: { hour: 8, minute: 0 },
      printFormat: ['', Validators.required],
      statusId: [2, Validators.required]
    });
  }

  getFormData() {
    this.standbyListData$ = this.masterDataService.getStandbyListSelection().subscribe((standbylistRes: StandbylistObject[]) => {
      this.standbylistSelectionData = standbylistRes;
    });
    this.userData$ = this.masterDataService.getUserlistDropdown().subscribe((userRes: UserDropdownObject[]) => {
      this.userSelectionData = userRes;
    });
    this.reportData$ = this.reportingService.getReportData().subscribe((reportRes: ReportObject[]) => {
      this.reportSelectionData = reportRes;
    });
  }

  generate() {
    if (FormUtil.validate(this.form)) {
      const formValue = this.form.getRawValue();
      FormUtil.formatDates(formValue, ['validFrom', 'validTo']);
      FormUtil.addTimeToDates(formValue);
      delete formValue.validFromTime;
      delete formValue.validToTime;

      if (this.isSearchingPerson) {
        formValue.userId = formValue.standById;
      } else {
        formValue.standByListId = formValue.standById;
      }
      delete formValue.standById;

      if (this.isSearchingPerson) {
        delete formValue.standByListId;
      } else {
        delete formValue.userId;
      }
      formValue.printFormat = formValue.printFormat.toLowerCase();
      const d = new Date();
      d.setTime( d.getTime() + d.getTimezoneOffset() * -1 * 60 * 1000);
      formValue.currentDate = d.toISOString();

      this.reportingService.generateReport(formValue).subscribe(response => {
        let mimeType = '';

        if (formValue.printFormat === 'pdf') {
          mimeType = 'application/pdf';
        } else if (formValue.printFormat === 'xlsx') {
          mimeType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet';
        }

        this.downLoadFile(
          response,
          mimeType,
          formValue.reportName,
          formValue.printFormat
        );
      });
    }
  }

  ngOnDestroy() {
    if (this.standbyListData$) {
      this.standbyListData$.unsubscribe();
    }
    if (this.userData$) {
      this.userData$.unsubscribe();
    }
    if (this.reportData$) {
      this.reportData$.unsubscribe();
    }

  }

  setReportDefaultDays(clicked: boolean) {
    const formValue = this.form.getRawValue();

    if (clicked) {
      const days = 7;
      const validFromDate: Date = new Date(formValue.date.validFrom.year, formValue.date.validFrom.month - 1, formValue.date.validFrom.day);
      FormUtil.setDefaultReportDate(this.form, 'validTo', days, validFromDate);
    } else {
      const days = 1;
      const validFromDate: Date = new Date(formValue.date.validFrom.year, formValue.date.validFrom.month - 1, formValue.date.validFrom.day);
      FormUtil.setDefaultReportDate(this.form, 'validTo', days, validFromDate);
    }
  }

  getUserId() {
    const username = this.authService.getUsername();
    this.userSelectionData.forEach(element => {
      const concatedName = element.firstname + ' ' + element.lastname;
      if (username === concatedName) {
        this.form.patchValue({
          standById: element.id
        });
      }
    });
  }

  setDefaultDate(field: string) {
    FormUtil.setDefaultDate(this.form, field);
  }

  ngOnInit() {
    this.createForm();
    this.form.controls.reportName.valueChanges.subscribe((val) => {
      if (val === 'Persönlicher_Einsatzplan') {
        this.isSearchingPerson = true;
        this.getUserId();
        this.form.patchValue({ standById: '' });
      } else {
        this.form.patchValue({ standById: '' });
        this.isSearchingPerson = false;
      }
    });
    this.getFormData();
    this.setDefaultDate('validFrom');
    this.setReportDefaultDays(false);
  }

  downLoadFile(data: any, type: string, reportName: string, printFormat: string) {

    const blob: Blob = new Blob([data], { type: type });
    const fileName: string = reportName + '.' + printFormat.toLocaleLowerCase();
    saveAs(blob, fileName);
  }

}

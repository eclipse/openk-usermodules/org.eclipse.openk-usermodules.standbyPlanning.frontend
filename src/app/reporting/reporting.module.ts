/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ReportingRoutingModule } from '@reporting/reporting-routing.module';
import { OverviewComponent } from '@reporting/components/overview/overview.component';
import { SharedModule } from '@shared/shared.module';
import { StandbyReportingComponent } from '@reporting/components/standby-reporting/standby-reporting.component';
import { DashboardComponent } from '@reporting/components/dashboard/dashboard.component';

@NgModule({
  imports: [
    CommonModule,
    ReportingRoutingModule,
    SharedModule
  ],
  declarations: [
    OverviewComponent,
    StandbyReportingComponent,
    DashboardComponent
  ]
})
export class ReportingModule { }

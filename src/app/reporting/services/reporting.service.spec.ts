/********************************************************************************
 * Copyright © 2018 Mettenmeier GmbH.
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v. 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { ReportingService } from './reporting.service';
import { UtilService } from '@core/services/util.service';

describe('ReportingService', () => {

  let httpMock: HttpTestingController;
  let reportingService: ReportingService;
  let utilService: UtilService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        ReportingService,
        UtilService
      ]
    });
    httpMock = TestBed.get(HttpTestingController);
    reportingService = TestBed.get(ReportingService);
    utilService = TestBed.get(UtilService);
  });

  it('should be created', inject([ReportingService], (service: ReportingService) => {
    expect(service).toBeTruthy();
  }));

  it('should get the reports Data', () => {
    reportingService.getReportData().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/report/list`);
  });

  it('should get the searchlist', () => {
    reportingService.getSearchList().subscribe(response => {
      expect(response).toBeTruthy();
    });

    httpMock.expectOne(`${utilService.readConfig('basePath')}/location/searchlist`);
  });
});

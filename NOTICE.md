# Notices for Eclipse openK User Modules

This content is produced and maintained by the Eclipse openK User Modules
project.

* Project home:
   https://projects.eclipse.org/projects/technology.openk-usermodules

## Trademarks

Eclipse openK User Modules is a trademark of the Eclipse Foundation.

## Copyright

All content is the property of the respective authors or their employers. For
more information regarding authorship of content, please consult the listed
source code repository logs.

## Declared Project Licenses

This program and the accompanying materials are made available under the terms
of the Eclipse Public License v. 2.0 which is available at
http://www.eclipse.org/legal/epl-2.0.

SPDX-License-Identifier: EPL-2.0

## Source Code

The project maintains the following source code repositories:

* http://git.eclipse.org/c/openk-usermodules/openk-usermodules.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.mics.centralService.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.mics.homeService.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.backend.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.plannedGridMeasures.frontend.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.backend.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.docu.git
* http://git.eclipse.org/c/openk-usermodules/org.eclipse.openk-usermodules.standbyPlanning.frontend.git

## Third-party Content

This project leverages the following third party content.

ag-grid (20.0.0)

* License: MIT
* Project: https://www.ag-grid.com/
* Source: https://github.com/ag-grid/ag-grid

ag-grid-angular (20.0.0)

* License: MIT
* Project: https://www.ag-grid.com/
* Source: https://github.com/ag-grid/ag-grid-angular

angular animations (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/guide/animations
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular common (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/api/common
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular compiler (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular core (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular forms (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular http (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular platform-browser (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular platform-browser-dynamic (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

angular router (6.1.0)

* License: Apache-2.0 AND MIT
* Project: https://angular.io/
* Source: https://github.com/angular/angular/releases/tag/6.1.0

auth0/angular-jwt (2.0.0)

* License: MIT
* Project: https://github.com/auth0/angular2-jwt
* Source: https://github.com/auth0/angular2-jwt/releases/tag/2.0.0

bootstrap (4.1.1)

* License: MIT
* Project: https://getbootstrap.com/
* Source: https://github.com/twbs/bootstrap

classlist.js (1.1.20150312)

* License: Unlicense

core-js (2.5.4)

* License: MIT
* Project: https://github.com/zloirock/core-js
* Source: https://github.com/zloirock/core-js/releases/tag/v2.5.4

font-awesome (4.7.0)

* License: OFL-1.1 AND MIT

ng-bootstrap (3.3.0)

* License: MIT
* Project: https://ng-bootstrap.github.io/#/home
* Source: https://github.com/ng-bootstrap/ng-bootstrap

primeicons (1.0.0)

* License: MIT
* Project: https://www.primefaces.org/primeng/#/
* Source: https://github.com/primefaces/primeicons

primeng (6.0.0)

* License: MIT
* Project: https://www.primefaces.org/primeng/#/
* Source: https://github.com/primefaces/primeng

primeng (6.1.3)

* License: MIT
* Project: https://www.primefaces.org/primeng/#/
* Source: https://github.com/primefaces/primeng

rxjs (6.2.2)

* License: Apache-2.0
* Project: https://github.com/Reactive-Extensions/RxJS
* Source: https://github.com/Reactive-Extensions/RxJS

urlrewritefilter (4.0.4)

* License: BSD-3-Clause AND (Apache-2.0 OR LGPL-2.0 OR GPL-2.0)
* Project: https://github.com/paultuckey/urlrewritefilter
* Source: https://github.com/paultuckey/urlrewritefilter

web-animations (2.3.1)

* License: Apache-2.0 AND BSD-3-Clause AND MIT
* Source: https://github.com/web-animations/web-animations-js

web-animations-js (2.3.1)

* License: Apache-2.0
* Project: https://github.com/web-animations/web-animations-js
* Source:
   https://github.com/web-animations/web-animations-js/releases/tag/2.3.1

zone.js (0.8.26)

* License: MIT

zone.js (0.8.26)

* License: MIT
* Source: https://github.com/angular/zone.js/releases/tag/v0.8.26

## Cryptography

Content may contain encryption software. The country in which you are currently
may have restrictions on the import, possession, and use, and/or re-export to
another country, of encryption software. BEFORE using any encryption software,
please check the country's laws, regulations and policies concerning the import,
possession, or use, and re-export of encryption software, to see if this is
permitted.